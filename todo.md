# Todos

## Calendar app ([*1])

- Calendar layout (only mondtly view) => http://jquense.github.io/react-big-calendar/examples/index.html
- Can create events
  - Title
  - Description
  - Cover image => if contains link, pull cover image from there
- Date, Duration
- Notify => Email notifications (possibly SMS with zillio later. Notify x - days/hours/minutes before event)
  - ++ Notify through push notifications
- Scheduled notifications
  - Scheduler heroku dyno ==> runs every 10 minutes
    OR
    Schedule emails with the SendGrid API https://sendgrid.com/docs/for-developers/sending-email/scheduling-email/
    OR both:
    Use the dyno in every 1h and send the notifications to sendgrid using scheduling ==> the idea is that no one will modify notifications 1 h before the notification sending is due
  - Runs an express script which sends the notifications
- Integrate events with native calendar App
- ++ Adding users to the event
- ++ Google reminder app with google notifications

## App ideas

- Drink water reminder app [*1]
- Boredom box: https://kwit.app/en/blog/posts/boredom-and-quitting-smoking-Kwit-s-tips-to-quit-without-getting-bored
- Stocks app
  - price alerts
- Real time chat app
- 1PW clone

### Crypto based social media app

- users can swap NFTs
- images as NFTs
- uses can gain crypto watching ads

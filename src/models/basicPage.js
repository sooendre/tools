const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const basicPage = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    slug: {
      type: String,
      required: true,
      unique: true,
    },
    body: {
      type: String,
      required: false,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("BasicPage", basicPage);

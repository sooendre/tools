const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const balanceSheetSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    users: {
      type: Array,
      required: true,
    },
    expenses: {
      type: Array,
      required: false,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("BalanceSheet", balanceSheetSchema);

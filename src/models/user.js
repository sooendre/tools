const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema(
  {
    username: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    active: {
      type: Boolean,
      required: true,
      default: false,
    },
    events: {
      type: Array,
      required: true,
      default: [],
    },
    notes: {
      type: Array,
      required: false,
      default: [],
    },
    passwordResetToken: {
      type: String,
      required: false,
    },
    permissions: {
      type: Array,
      required: false,
      default: [],
    },
  },
  { timestamps: true }
);

UserSchema.method({
  /**
   *
   * @param {Array|String} permissions the permission(s) to check
   *
   * @returns {Boolean} true if user has permission false otherwise
   */
  hasPermissions: function (params) {
    const { permissions } = params;
    const checkPermission = (search) => this.permissions.indexOf(search) < 0;

    if (!Array.isArray(permissions)) return checkPermission(permissions);
    else {
      let check = true;
      permissions.forEach((permission) => {
        if (!checkPermission(permission)) check = false;
      });

      return check;
    }
  },

  /**
   * TODO: finish me
   * @param {String} username
   * @returns {Boolean}
   */
  validateUsername: async (username) => {
    if (username.length <= 5)
      throw new Error("Username must be at least 5 characters long.");
    const userExists = await User.find({ username: username });
    if (userExists.length > 0) throw new Error("This username already exists!");

    return true;
  },
});

module.exports = mongoose.model("User", UserSchema);

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const NotificationSchema = new Schema(
  {
    type: {
      type: Number,
      required: true,
    },
    date: {
      type: Date,
      required: false,
    },
    sent: {
      type: Boolean,
      required: true,
      default: false,
    },
  },
  { timestamps: true }
);

NotificationSchema.static({
  /**
   * Process notification
   * Convert notification input into array
   *
   * @param {Date} startDate the start date of the event
   * @param {Array} notifications the new notifications we want to add to the event
   *
   * @returns {Array} the array of notifications
   * @throws {Error} if any of the notifications are not properly set
   */
  processNotification: (startDate, notifications) => {
    let validNotifications = new Array();
    const now = new Date().getTime();
    if (Array.isArray(notifications) && notifications.length > 0) {
      notifications.forEach((notification) => {
        if (notification.date > startDate)
          throw new Error("You cannot be notified after the event started.");

        if (notification.date <= now)
          throw new Error("Notification date is in the past");

        validNotifications.push(notification);
      });
    }
    return validNotifications;
  },
});

module.exports = mongoose.model("Notification", NotificationSchema);

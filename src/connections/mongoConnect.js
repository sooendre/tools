const mongoose = require("mongoose");

const mongoURI = `mongodb${
  process.env.NODE_ENV == "production" ? "+srv" : ""
}://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@${
  process.env.MONGO_HOST
}/${process.env.MONGO_DB}?retryWrites=true&w=majority`;

const connection = mongoose.connect(mongoURI, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});
module.exports = connection;

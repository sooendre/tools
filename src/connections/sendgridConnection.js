const sgMail = require("@sendgrid/mail");
const { dateFormat } = require("./../utils/Tools");
const appBaseUrl = process.env.APP_URL || "http://localhost";

module.exports = {
  sendPassReset: (email, username, token) => {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const msg = {
      to: email,
      from: process.env.APP_EMAIL_ADDRESS, // Use the email address or domain you verified above
      templateId: "d-d3d3c3e8c08748c298a84abddbfc9662",
      dynamicTemplateData: {
        username: username,
        passResetUrl: `${appBaseUrl}/user/password-reset?email=${email}&token=${token}`,
      },
    };
    (async () => {
      try {
        await sgMail.send(msg);
        return true;
      } catch (error) {
        console.error(error);

        if (error.response) {
          console.error(error.response.body);
        }
        return false;
      }
    })();
  },

  /**
   *
   * @param {Object} event: the event, date: event notification date}
   * @returns {Boolean} true if email was sent, false otherwise
   */
  sendNotificationEmail: async ({ event, date }) => {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);

    try {
      await sgMail.send({
        to: user.email,
        from: process.env.APP_EMAIL_ADDRESS, // Use the email address or domain you verified above
        templateId: "d-766b0362657748c59b1eaeca77eeb10b",
        dynamicTemplateData: {
          title: event.title,
          dateTime: dateFormat(event.startDate, "H:mm, Do MMMM YYYY"),
          description: event.description || "",
          viewEventURL: `${appBaseUrl}/user/events/${event._id}`,
        },
        sendAt: parseInt(date / 1000), // in UNIX timestamp seconds, not milliseconds
        batchId: event._id, // add a batchId to the email so we can pause or cancel the email later
      });
      // TODO: save sent notification so sending can be stopped
      // change sent status to true so we know it's been fired
    } catch (error) {
      if (error.response) {
        console.error(error.response.body);
      }
      return false;
    }
    return true;
  },
};

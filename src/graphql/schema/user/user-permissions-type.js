const gql = require("graphql-tag");

module.exports = gql`
  enum UserPermissionEnum {
    LIST_ACTIVITIES
    ADMINISTER_ACTIVITIES
    LIST_USERS
  }
`;

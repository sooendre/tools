const User = require("../../../models/user");
const jsonwebtoken = require("jsonwebtoken");
const Password = require("../../../utils/Password");
const { sendPassReset } = require("../../../connections/sendgridConnection");
const emailValidator = require("email-validator");
const { authenticateUser } = require("../../../utils/Authentication");
const { validate: passValidator } = require("../../../utils/Password");
require("dotenv").config();

module.exports = {
  Query: {
    findUser: async (parent, { search }, context) => {
      authenticateUser(context);
      if (search.length > 2) {
        return await User.find({
          $and: [
            { active: true },
            {
              $or: [
                { username: { $regex: ".*" + search + ".*" } },
                { email: { $regex: ".*" + search + ".*@.*" } },
              ],
            },
          ],
        }).limit(8);
      }
    },

    getCurrentUser: async (parent, args, context) => {
      return authenticateUser(context);
    },
  },

  Mutation: {
    createUser: async (parent, args) => {
      // temporarily close registration
      throw new Error("Registration is closed");

      const { username, email, password, firstName, lastName } = args.user;
      const hashedPass = await Password.hash(password);

      // validate email address
      if (!emailValidator.validate(email))
        throw new Error("Invalid email address!");

      const emailExists = await User.find({ email: email });
      if (emailExists.length > 0) throw new Error("This email already exists!");

      const user = new User({
        username,
        email,
        password: hashedPass,
        firstName,
        lastName,
        active: false,
      });

      await user.validateUsername(username);

      const newUser = await user.save();
      return { ...newUser._doc, _id: newUser.id };
    },

    login: async (parent, args) => {
      const { email, password, remember } = args;
      const loginError = "User not found";
      const user = await User.findOne({ email: email, active: true });
      if (!user) throw new Error(loginError);

      const isValid = await Password.compare(password, user.password);
      if (!isValid) throw new Error(loginError);

      // return jwt
      const token = jsonwebtoken.sign(
        { id: user.id, email: user.username },
        process.env.JWT_SECRET,
        { expiresIn: remember == true ? "30d" : "1d" } // TODO: login threshold in .env
      );

      return {
        token,
        user,
      };
    },

    /**
     *
     * @inheritdoc
     *
     * 1.) get current user to update
     * 2.) check fields exists, and validity
     * 3.) update
     */
    updateUser: async (parent, args, context) => {
      const { email, currentPassword, newPassword, firstName, lastName } =
        args.user;

      const updateUser = authenticateUser(context);

      if (firstName !== undefined && firstName !== "")
        updateUser.firstName = firstName;

      if (lastName !== undefined && lastName !== "")
        updateUser.lastName = lastName;

      if (email !== undefined && email != updateUser.email) {
        if (!emailValidator.validate(email))
          throw new Error("Invalid email address!");

        const emailExists = await User.find({ email: email });
        if (emailExists.length > 0)
          throw new Error("This email already exists!");

        updateUser.email = email;
      }
      if (
        currentPassword !== undefined &&
        newPassword !== undefined &&
        currentPassword != newPassword
      ) {
        const currentPassValid = await Password.compare(
          currentPassword,
          updateUser.password
        );
        if (!currentPassValid)
          throw new Error("Your current password is invalid.");
        if (!passValidator(newPassword))
          throw new Error(
            "New password does not meet the complexity criteria."
          );

        updateUser.password = await Password.hash(newPassword);
      }

      const newUser = await updateUser.save();
      return { ...newUser._doc, _id: newUser.id };
    },
    /**
     *
     * @inheritdoc
     *
     * 1.) check if email exists
     * 2.) if so, add pass reset token to user object
     * 3.) send email to user
     */
    sendPasswordReset: async (parent, { email }) => {
      const user = await User.findOne({ email: email });
      if (user != null) {
        const token = require("crypto").randomBytes(32).toString("hex");
        user.passwordResetToken = token;
        await user.save();
        await sendPassReset(email, user.username, token);
      }

      return { message: "Password reset email sent!" };
    },

    /**
     *
     * @inheritdoc
     *
     * 1.) check if email exists
     * 2.) check if token is correct
     * 3.) validate password
     * 4.) save new password
     */
    resetPassword: async (parent, { email, password, rePassword, token }) => {
      if (password != rePassword) throw new Error("Passwords don't match");

      const updateUser = await User.findOne({ email: email });

      if (
        !updateUser ||
        !updateUser.passwordResetToken ||
        updateUser.passwordResetToken != token
      )
        throw new Error("Invalid password reset link.");

      if (!passValidator(password))
        throw new Error("New password does not meet the complexity criteria.");

      updateUser.password = await Password.hash(password);
      updateUser.passwordResetToken = null;
      await updateUser.save();

      return { message: "Password reset successfully!" };
    },

    // end Mutations
  },
};

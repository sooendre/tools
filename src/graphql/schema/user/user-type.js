const gql = require("graphql-tag");
const { mergeTypeDefs } = require("@graphql-tools/merge");
const permissionsEnum = require("./user-permissions-type");

module.exports = mergeTypeDefs([
  permissionsEnum,
  gql`
    type User {
      _id: ID!
      username: String!
      email: String!
      password: String!
      firstName: String!
      lastName: String!
      createdAt: String!
      updatedAt: String!
      lastLogin: String!
    }

    input UserInput {
      username: String!
      email: String!
      password: String!
      firstName: String!
      lastName: String!
      createdAt: String
      updatedAt: String
      lastLogin: String
    }

    input UpdateUserInput {
      email: String
      currentPassword: String
      newPassword: String
      firstName: String
      lastName: String
    }

    type AuthPayload {
      token: String!
      user: User!
    }

    type PassResetPayload {
      message: String!
    }

    type Query {
      findUser(search: String!): [User]
      getCurrentUser: User!
    }

    type Mutation {
      createUser(user: UserInput): User
      updateUser(user: UpdateUserInput): User
      sendPasswordReset(email: String!): PassResetPayload
      resetPassword(
        email: String!
        password: String!
        rePassword: String!
        token: String!
      ): PassResetPayload
      login(email: String!, password: String!, remember: Boolean): AuthPayload!
    }
  `,
]);

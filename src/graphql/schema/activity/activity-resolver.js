const Activity = require("../../../models/activity");
const { authenticateUser } = require("../../../utils/Authentication");

module.exports = {
  Query: {
    getRandomActivity: async (parent, { duration }, context) => {
      authenticateUser(context);

      let durationFilter = { $gt: 0 };
      switch (duration) {
        case "480":
          durationFilter = { $gt: 120, $lte: 480 };
          break;
        case "120":
          durationFilter = { $gt: 60, $lte: 120 };
          break;
        case "60":
          durationFilter = { $gt: 30, $lte: 60 };
          break;
        case "30":
          durationFilter = { $lte: 30 };
          break;
      }

      // return a random
      const result = await Activity.aggregate([
        { $match: { duration: durationFilter } },
        { $sample: { size: 1 } },
      ]);

      return result[0] || null;
    },

    getActivity: async (parent, { activityID }, context) => {
      authenticateUser(context);

      return await Activity.findOne({ _id: activityID });
    },

    getActivities: async (parent, args, context) => {
      const authUser = authenticateUser(context);

      const check = authUser.hasPermissions("LIST_ACTIVITIES");
      if (!check) throw new Error(`You're not allowed to do that.`);

      return await Activity.find({});
    },
  },

  Mutation: {
    createActivity: async (parent, { payload }, context) => {
      const { name, description, duration } = payload;
      authenticateUser(context);
      const activity = new Activity({ name, description, duration });
      await activity.save();

      return activity;
    },

    updateActivity: async (parent, { payload }, context) => {
      const authUser = authenticateUser(context);
      const check = authUser.hasPermissions("ADMINISTER_ACTIVITIES");
      if (!check) throw new Error(`You're not allowed to do that.`);

      const { activityID, name, description, duration } = payload;

      const activity = await Activity.findOne({ _id: activityID });
      if (!activity) throw new Error(`Activity not found.`);

      if (name !== undefined) activity.name = name;
      if (description !== undefined) activity.description = description;
      if (duration !== undefined) activity.duration = duration;

      await activity.save();

      return activity;
    },

    deleteActivity: async (parent, { activityID }, context) => {
      const authUser = authenticateUser(context);
      const check = authUser.hasPermissions("ADMINISTER_ACTIVITIES");
      if (!check) throw new Error(`You're not allowed to do that.`);

      const activity = await Activity.findOne({ _id: activityID });
      if (!activity) throw new Error(`Activity not found.`);

      await activity.remove();
      return { _id: activityID };
    },
  },
};

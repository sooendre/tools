const gql = require("graphql-tag");

module.exports = gql`
  type Activity {
    _id: ID!
    name: String!
    description: String
    duration: Int
    createdAt: String
    updatedAt: String
  }

  input CreateActivityInput {
    name: String!
    description: String
    duration: Int
  }

  input UpdateActivityInput {
    activityID: ID!
    name: String
    description: String
    duration: Int
  }

  type Query {
    getRandomActivity(duration: String): Activity
    getActivity(activityID: ID!): Activity
    getActivities: [Activity]
  }

  type Mutation {
    createActivity(payload: CreateActivityInput!): Activity!
    updateActivity(payload: UpdateActivityInput!): Activity!
    deleteActivity(activityID: ID!): DeletedPayload!
  }
`;

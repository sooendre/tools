const gql = require("graphql-tag");

module.exports = gql`
  type BalanceSheet {
    _id: ID!
    title: String!
    users: [User!]
    expenses: [Expense!]
    createdAt: String!
  }

  type Expense {
    _id: ID!
    userID: String!
    value: Float!
    createdAt: String!
  }

  type DeletedPayload {
    _id: ID!
  }

  input UpdateExpenseInput {
    expenseID: ID!
    value: Float!
  }

  input BalanceSheetInput {
    title: String!
    users: [String!]
  }

  type Query {
    balanceSheets(balanceID: ID): [BalanceSheet!]
  }

  type Mutation {
    createBalanceSheet(balanceSheet: BalanceSheetInput!): BalanceSheet
    updateBalanceSheet(
      balanceID: ID!
      title: String
      users: [String]
    ): BalanceSheet

    deleteBalanceSheet(balanceID: ID!): DeletedPayload

    addExpense(balanceID: ID!, value: Float!): Expense

    updateExpense(balanceID: ID!, expense: UpdateExpenseInput!): Expense

    deleteExpense(balanceID: ID!, expenseID: ID!): DeletedPayload
  }
`;

const BalanceSheet = require("../../../models/balanceSheet");
const Expense = require("../../../models/expense");
const User = require("../../../models/user");
const { authenticateUser } = require("../../../utils/Authentication");
const { arrayUnique, isNumeric } = require("../../../utils/Tools");
const mongoose = require("mongoose");

const balanceHasUser = (balance, userID) => {
  if (balance.users.indexOf(userID) < 0)
    throw new Error("You don't have permission to edit this balance sheet!");
  else return true;
};

const expenseHasUser = (expense, userID) => {
  if (expense.userID != userID)
    throw new Error("You don't have permission to edit this expense!");
  else return true;
};

module.exports = {
  Query: {
    balanceSheets: async (parent, args, context) => {
      const authUser = authenticateUser(context);

      // if filtering by ID add filter to lookup
      const { balanceID } = args;
      const balanceFilter = {
        users: { $in: [authUser._id] },
      };
      if (balanceID) balanceFilter._id = mongoose.Types.ObjectId(balanceID);

      const results = await BalanceSheet.aggregate([
        {
          $match: balanceFilter,
        },
        {
          $lookup: {
            localField: "users",
            from: "users",
            foreignField: "_id",
            as: "users",
          },
        },
        {
          $sort: {
            createdAt: -1,
          },
        },
      ]);

      return results;
    },
  },

  Mutation: {
    createBalanceSheet: async (parent, { balanceSheet }, context) => {
      const authUser = authenticateUser(context);
      const { title, users } = balanceSheet;

      if (title.trim() === "") throw new Error("Empty title is invalid!");

      // add current user to list
      users.push(`${authUser._id}`);
      const sheet = new BalanceSheet({
        title,
        users: arrayUnique(users).map((user) => mongoose.Types.ObjectId(user)),
      });
      const newSheet = await sheet.save();

      // Join the user object ot the balance
      const newSheetUsers = await User.find({
        _id: { $in: newSheet.users },
      });

      sheet.users = newSheetUsers;

      return newSheet;
    },

    updateBalanceSheet: async (
      parent,
      { balanceID, title, users },
      context
    ) => {
      const authUser = authenticateUser(context);

      const sheet = await BalanceSheet.findOne({ _id: balanceID });

      // check if auth user has permission to edit it
      balanceHasUser(sheet, authUser._id);

      // update title
      if (title !== undefined) {
        if (title.trim() === "") throw new Error("Empty title is invalid!");
        sheet.title = title;
      }

      if (users !== undefined) {
        if (users === [])
          throw new Error("Balance Sheet must belong to a user!");

        sheet.users = arrayUnique(users).map((user) =>
          mongoose.Types.ObjectId(user)
        );
      }

      await sheet.save();

      // Join the user object ot the balance
      const newSheetUsers = await User.find({
        _id: { $in: sheet.users },
      });

      sheet.users = newSheetUsers;

      return sheet;
    },

    deleteBalanceSheet: async (parent, { balanceID }, context) => {
      const authUser = authenticateUser(context);
      const sheet = await BalanceSheet.findOne({ _id: balanceID });
      balanceHasUser(sheet, authUser._id);

      return await BalanceSheet.findByIdAndDelete({ _id: balanceID });
    },

    /**
     * TODO: add user to balance sheet
     * pending invitations in user object
     * if invitation accepted move user to list
     *
     * Notifications > Sendgrid TODO
     * */

    addExpense: async (parent, { balanceID, value }, context) => {
      if (!isNumeric(value)) throw new Error("Not a number!");
      const authUser = authenticateUser(context);
      const sheet = await BalanceSheet.findOne({ _id: balanceID });
      balanceHasUser(sheet, authUser._id);

      const newExpense = new Expense({
        value,
        userID: authUser._id,
        createdAt: Date.now(),
      });
      if (!sheet.expenses) sheet.expenses = [];
      sheet.expenses.unshift(newExpense);

      await sheet.save();
      return newExpense;
    },

    // Expense Mutations
    updateExpense: async (parent, { balanceID, expense }, context) => {
      const authUser = authenticateUser(context);
      const { expenseID, value } = expense;

      // validate expense value
      if (!isNumeric(value))
        throw new Error("Expense must have numeric value.");

      const sheet = await BalanceSheet.findOne({ _id: balanceID });

      // check if user has permission to update
      balanceHasUser(sheet, authUser._id);

      // Find updating expense
      let updateIndex = -1;
      const updatedData = sheet.expenses.find((item, index) => {
        if (item._id == expenseID) {
          expenseHasUser(item, authUser._id);
          item.value = value;
          updateIndex = index;
          return item;
        }
      });
      if (updateIndex < 0) throw new Error("Expense not found!");

      // update the expense
      const updatedExpense = await new Expense(updatedData);
      sheet.expenses.set(updateIndex, updatedData);

      await sheet.save();
      return updatedExpense;
    },

    deleteExpense: async (parent, { balanceID, expenseID }, context) => {
      const authUser = authenticateUser(context);
      const sheet = await BalanceSheet.findOne({ _id: balanceID });

      // check if user has permission to update
      balanceHasUser(sheet, authUser._id);

      // filter and delete expense with ID
      sheet.expenses = sheet.expenses.filter((expense) => {
        if (expense._id == expenseID) {
          expenseHasUser(expense, authUser._id);
          return false;
        } else return true;
      });

      await sheet.save();
      return { _id: expenseID };
    },
  },
};

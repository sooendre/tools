const Note = require("../../../models/note");
const { authenticateUser } = require("../../../utils/Authentication");

module.exports = {
  Query: {
    getUserNotes: async (parent, args, context) => {
      const authUser = authenticateUser(context);
      return authUser.notes;
    },
    getUserNote: async (parent, { noteID }, context) => {
      const authUser = authenticateUser(context);

      const note = authUser.notes.find((item) => item._id == noteID);

      return note;
    },
  },

  Mutation: {
    createNote: async (parent, { payload }, context) => {
      const { title, description, color } = payload;

      const authUser = authenticateUser(context);
      const newNote = new Note({ title, description, color });
      authUser.notes.push(newNote);
      await authUser.save();

      return newNote;
    },

    updateNote: async (parent, { payload }, context) => {
      const { title, noteID, description, color } = payload;
      const authUser = authenticateUser(context);
      let noteIndex = -1;
      const updateNote = authUser.notes.find((item, index) => {
        if (item._id == noteID) {
          noteIndex = index;

          if (title !== undefined) item.title = title;

          if (description !== undefined) item.description = description;

          if (color !== undefined) item.color = color;

          return item;
        }
      });

      if (noteIndex < 0) throw new Error("Note not found!");

      authUser.notes.set(noteIndex, updateNote);
      await authUser.save();

      return updateNote;
    },

    deleteNote: async (parent, { noteID }, context) => {
      const authUser = authenticateUser(context);

      // create new notes array w/o the deleted note
      authUser.notes = authUser.notes.filter((item) => item._id != noteID);

      await authUser.save();
      return { _id: noteID };
    },
  },
};

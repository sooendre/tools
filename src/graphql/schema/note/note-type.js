const gql = require("graphql-tag");

module.exports = gql`
  type Note {
    _id: ID!
    title: String!
    description: String
    color: String
    createdAt: String
    updatedAt: String
  }

  input CreateNoteInput {
    title: String!
    description: String
    color: String
  }

  input UpdateNoteInput {
    noteID: ID!
    title: String
    description: String
    color: String
  }

  type Query {
    getUserNotes: [Note]
    getUserNote(noteID: ID!): Note
  }

  type Mutation {
    createNote(payload: CreateNoteInput!): Note!
    updateNote(payload: UpdateNoteInput!): Note!
    deleteNote(noteID: ID!): DeletedPayload!
  }
`;

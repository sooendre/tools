const Event = require("../../../models/event");
const Notification = require("../../../models/notification");
const User = require("../../../models/user");
const { authenticateUser } = require("../../../utils/Authentication");
const {
  sendNotificationEmail,
} = require("../../../connections/sendgridConnection");

module.exports = {
  Query: {
    getUserEvents: async (parent, args, context) => {
      const authUser = authenticateUser(context);
      return authUser.events;
    },
    getUserEvent: async (parent, { eventID }, context) => {
      const authUser = authenticateUser(context);

      const note = authUser.events.find((item) => item._id == eventID);

      return note;
    },
  },

  Mutation: {
    createEvent: async (parent, { payload }, context) => {
      const authUser = authenticateUser(context);

      const { title, description, startDate, endDate, notifications } = payload;

      if (title.trim() === "") throw new Error("Empty title is invalid!");
      if (endDate !== undefined && startDate >= endDate)
        throw new Error("Start date must be behind End date");

      const validNotifications = Notification.processNotification(
        startDate,
        notifications
      );

      const event = new Event({
        title,
        description,
        startDate,
        endDate,
        notifications: validNotifications,
      });

      // prepare user.events if this is the first event for the user
      if (authUser.events === undefined) authUser.events = new Array();

      authUser.events.push(event);
      await authUser.save();
      return event;
    },

    updateEvent: async (parent, { payload }, context) => {
      const authUser = authenticateUser(context);
      const { eventID, title, description, startDate, endDate, notifications } =
        payload;

      let updateIndex = -1;
      const updateData = authUser.events.find((item, index) => {
        if (item._id == eventID) {
          // update filtered event with user input
          if (title !== undefined) item.title = title;

          if (description !== undefined) item.description = description;

          if (startDate !== undefined) item.startDate = startDate;

          if (endDate !== undefined) item.endDate = endDate;

          if (
            item.endDate !== undefined &&
            item.endDate !== null &&
            item.startDate >= item.endDate
          )
            throw new Error("Start date must be behind End date");

          if (notifications !== undefined)
            item.notifications = Notification.processNotification(
              item.startDate,
              notifications
            );
          else {
            // we hve to compare notifications to the potentially new start date
            item.notifications = Notification.processNotification(
              item.startDate,
              item.notifications
            );
          }

          // keep the index so we can update soon
          updateIndex = index;
          return item;
        }
      });

      if (updateIndex < 0) throw new Error("Event not found!");

      const updateEvent = await new Event(updateData);
      authUser.events.set(updateIndex, updateEvent);

      await authUser.save();
      return updateEvent;
    },

    deleteEvent: async (parent, { eventID }, context) => {
      const authUser = authenticateUser(context);

      // create new events array w/o the deleted event
      authUser.events = authUser.events.filter((item) => item._id != eventID);

      await authUser.save();
      return { _id: eventID };
    },

    sendNotification: async (parent, { token }, context) => {
      let message = "Success!";

      // TODO: validate token

      // gather notifications for all uses due in the next hour
      const now = new Date().getTime();
      const hourFromNow = now + 3600000;

      const results = await User.find({
        "events.notifications.date": {
          $gte: now,
          $lte: hourFromNow,
        },
      });

      results.forEach(async (user) => {
        if (user.events && user.events.length)
          user.events.forEach(async (event) => {
            event.notifications.forEach(async (notification) => {
              if (notification.type === "EMAIL") {
                notification.sent = await sendNotificationEmail({
                  event,
                  date: notification.date,
                });
              }
            });
          });
      });
      // send email/sms/notification to the phone
      // save fired notifications in the user object

      return { message };
    },
  },
};

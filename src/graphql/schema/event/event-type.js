const gql = require("graphql-tag");

module.exports = gql`
  type Event {
    _id: ID!
    title: String!
    description: String
    startDate: Date!
    endDate: Date
    notifications: [Notification]
    createdAt: String
    updatedAt: String
  }

  input CreateEventInput {
    title: String!
    description: String
    startDate: Date!
    endDate: Date
    notifications: [NotificationInput]
  }

  input UpdateEventInput {
    eventID: ID!
    title: String
    description: String
    startDate: Date
    endDate: Date
    notifications: [NotificationInput]
  }

  type MessageResponse {
    message: String!
  }

  type Query {
    getUserEvents: [Event]
    getUserEvent(eventID: ID!): Event
  }
  type Mutation {
    createEvent(payload: CreateEventInput!): Event
    updateEvent(payload: UpdateEventInput!): Event
    deleteEvent(eventID: ID!): DeletedPayload!
    sendNotification(token: String!): MessageResponse!
  }
`;

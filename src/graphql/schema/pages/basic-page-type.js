const gql = require("graphql-tag");

module.exports = gql`
  type BasicPage {
    _id: ID!
    title: String!
    slug: String!
    body: String!
    createdAt: String
    updatedAt: String
  }

  input BasicPageInput {
    title: String!
    slug: String
    body: String!
  }

  type Query {
    getBasicPageBySlug(slug: String!): BasicPage
  }

  type Mutation {
    createBasicPage(payload: BasicPageInput!): BasicPage
  }
`;

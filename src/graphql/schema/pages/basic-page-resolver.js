const BasicPage = require("../../../models/basicPage");
const { authenticateUser } = require("../../../utils/Authentication");
const { generateSlug } = require("../../../utils/Tools");
require("dotenv").config();
const slugify = require("slugify");

module.exports = {
  Query: {
    getBasicPageBySlug: async (parent, { slug }) => {
      const results = await BasicPage.findOne({ slug: slug });

      console.log(results);

      return results;
    },
  },

  Mutation: {
    createBasicPage: async (parent, { payload }, context) => {
      authenticateUser(context);

      const { title, slug, body } = payload;

      if (title.trim() === "") throw new Error("Empty title is invalid!");
      if (body.trim() === "") throw new Error("Empty body is invalid!");

      const generatedSlug = await generateSlug(
        slug ? slug.toLowerCase() : slugify(title.toLowerCase())
      );

      const page = new BasicPage({
        title,
        slug: generatedSlug,
        body,
      });
      const newPage = await page.save();

      return newPage;
    },
  },
};

const gql = require("graphql-tag");

module.exports = gql`
  scalar Date

  enum NotificationType {
    PUSH
    EMAIL
    SMS
  }

  input NotificationInput {
    type: NotificationType!
    date: Date!
  }

  type Notification {
    type: NotificationType!
    date: Date!
  }
`;

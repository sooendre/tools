const jwt = require("jsonwebtoken");
const User = require("./../models/user");

module.exports = {
  authMiddleware: async (req, res, next) => {
    // Auth by: https://blog.pusher.com/handling-authentication-in-graphql/
    const bearer = req.headers.authorization;
    if (bearer) {
      const token = bearer.replace("Bearer ", "");

      jwt.verify(token, process.env.JWT_SECRET, async (err, decoded) => {
        if (err) {
          res.status(401).json({ message: "Login expired!" });
        }
        // this is the decoded JWT token containing the data added in jwt.sign({})
        req.user = await User.findOne({ _id: decoded.id, active: true });
        next();
      });
    } else {
      next();
    }
  },
};

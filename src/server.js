const express = require("express");
require("dotenv").config();
const glob = require("glob");
const cors = require("cors");
const { graphqlHTTP } = require("express-graphql");
const {
  makeExecutableSchema,
  mergeResolvers,
  mergeTypeDefs,
} = require("graphql-tools");
const { authMiddleware } = require("./middleware/authMiddleware");
const rateLimit = require("express-rate-limit");

const isDev = process.env.NODE_ENV == "development";
const appPort = process.env.PORT || 3000;
const app = express();
app.use(cors());

//iterate through resolvers file in the folder "graphql/folder/folder/whatever*-resolver.js"
let resolvers = glob.sync("src/graphql/*/*/*-resolver.js");
let registerResolvers = [];
for (const resolver of resolvers) {
  // add resolvers to array
  registerResolvers = [...registerResolvers, require("./../" + resolver)];
}

//iterate through resolvers file in the folder "graphql/folder/folder/whatever*-type.js"
let types = glob.sync("src/graphql/*/*/*-type.js");
let registerTypes = [];
for (const type of types) {
  // add types to array
  registerTypes = [...registerTypes, require("./../" + type)];
}

//make schema from typeDefs and Resolvers with "graphql-tool package (makeExecutableSchema)"
const schema = makeExecutableSchema({
  typeDefs: mergeTypeDefs(registerTypes), //merge array types
  resolvers: mergeResolvers(registerResolvers), //merge resolver type
});

// mongodb connection if you prefer mongodb
require("./connections/mongoConnect.js");
// end mongodb connection

const limiter = rateLimit({
  windowMs: process.env.RATE_LIMIT_WINDOW_MS || 15 * 60 * 1000, // 15 minutes
  max: process.env.RATE_LIMIT_MAX || 100, // limit each IP to 10 requests per windowMs
});

//  apply to all requests
app.use("/graphql", limiter);

//Make it work with express "express and express-graphql packages"
app.use(
  "/graphql",
  authMiddleware,
  graphqlHTTP((req) => ({
    schema: schema,
    graphiql: isDev,
    context: {
      user: req.user,
    },
  }))
);

// robots.txt
app.get("/robots.txt", function (req, res) {
  res.type("text/plain");
  res.send("User-agent: *\nDisallow: /");
});

app.listen(appPort, function () {
  console.log("🚀 Running a GraphQL API server at http://localhost:" + appPort);
});
if (isDev) {
  console.log("🚀 Running an Express GraphQL server at http://localhost:8081 ");
}

const bcrypt = require("bcrypt");
const passwordValidator = require("password-validator");

const saltRounds = 10;
// Create a schema
const schema = new passwordValidator();

// Add properties to it
// prettier-ignore
schema
.is().min(8)                                    // Minimum length 8
.is().max(100)                                  // Maximum length 100
.has().uppercase()                              // Must have uppercase letters
.has().lowercase()                              // Must have lowercase letters
.has().digits(1)                                // Must have at least a digits
.has().symbols(1)                                // Must have at least a symbols
.has().not().spaces()                           // Should not have spaces
.is().not().oneOf(['Passw0rd', 'Password123']); // Blacklist these values

module.exports = {
  hash: async (password) => {
    return await bcrypt.hash(password, saltRounds).then(function (hash) {
      return hash;
    });
  },

  compare: async (loginPass, password) => {
    return await bcrypt.compare(loginPass, password);
  },

  validate: (password) => {
    return schema.validate(password);
  },
};

const BasicPage = require("./../models/basicPage");
const moment = require("moment");

module.exports = {
  arrayUnique: (array) => {
    const set = new Set(array);
    const iterator = set.values();
    return Array.from(iterator);
  },
  isNumeric: (str) => {
    if (typeof str == "number") return true;
    if (typeof str != "string") return false; // we only process strings!
    return (
      !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
      !isNaN(parseFloat(str))
    ); // ...and ensure strings of whitespace fail
  },

  generateSlug: async (slug, iteration = 1, originalSlug = null) => {
    // check if node exists with current slug
    const slugExists = await BasicPage.findOne({ slug: slug });

    // slug doesn't exits we can use it
    if (slugExists === null) return slug;
    else {
      // if originalShrug doesn't exist set it to the current slug
      if (originalSlug === null) originalSlug = slug;

      return await module.exports.generateSlug(
        `${originalSlug}-${iteration}`,
        iteration++,
        originalSlug
      );
    }
  },

  /**
   * Converts date to the provided format
   *
   * @param {String} date the date string we want to transform
   * @param {Format} format string like Y-M-d
   *
   * @returns {String} formatted date string
   */
  dateFormat: (date, format) => {
    return moment(date).format(format);
  },
};

module.exports = {
  authenticateUser: (context) => {
    if (!context.user || !context.user._id) throw new Error("Unauthorised");

    return context.user;
  },
};
